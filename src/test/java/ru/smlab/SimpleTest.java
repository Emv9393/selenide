package ru.smlab;

import org.testng.annotations.Test;
import ru.smlab.pages.MainPages;
import ru.smlab.pages.MakeAnOrder;
import ru.smlab.pages.SearchBuyProduct;
import ru.smlab.pages.ShoppingCart;

public class SimpleTest {

    String name = "Евгения";
    String familyName = "Меньшикова";
    String nameProduct = "КЛЛАРИОН";

    @Test
    public void test (){
        MainPages mainPages = new MainPages();
        mainPages
                .openPage()
                .searchObject()
                .closeCartPanel();

        SearchBuyProduct searchBuyProduct = new SearchBuyProduct();
        searchBuyProduct
                .searchProduct(nameProduct)
                .buyProduct(nameProduct)
                .transitionCart();

        ShoppingCart shoppingCart = new ShoppingCart();
        shoppingCart
                .decrease(nameProduct)
                .order();

        MakeAnOrder makeAnOrder = new MakeAnOrder();
        makeAnOrder
                .checkout()
                .detailsPayment(name,familyName);


    }

}
