package ru.smlab.pages;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.*;

public class MainPages {


    public MainPages openPage(){
        open("https://testqastudio.me/");

        return this;
    }

    public MainPages searchObject(){
        SelenideElement searchElement = $x("//li[contains(@class,'layout-v2 product')][3]");
        searchElement.hover();
        SelenideElement basket = $x("//li[contains(@class,'layout-v2 product')][3]/div[@class='product-inner']/div[@class='product-thumbnail']/div[@class='product-loop__buttons']/a[contains(@aria-label, 'в корзину')]");
        basket.hover();
        basket.click();

        return this;
    }

    public MainPages closeCartPanel(){
        SelenideElement closeButton = $x("//div[contains(@class, 'cart-panel-content panel-content')]//div[@class='modal-header']//a[contains(@class,'button-close')]");
        closeButton.click();


        return this;
    }

}
