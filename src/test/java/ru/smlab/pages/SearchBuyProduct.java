package ru.smlab.pages;

import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.*;

public class SearchBuyProduct {

    public SearchBuyProduct searchProduct(String nameProduct){
        SelenideElement search = $x("//div[@class='site']//header[@class='site-header']//div[contains(@class,'menu-center')]//div[contains(@class,'container')]//div[contains(@class,'header-right-items')]//div[contains(@class, 'search-type-icon')]//span[@data-target='search-modal']");
        search.click();
        SelenideElement inputTextProduct = $x("//div[@class='search-panel-content panel-content']//div[@class='modal-content']//form[@class='form-search']//div[@class='search-wrapper']//input[@placeholder='Поиск предметов']");
        inputTextProduct.setValue(nameProduct);
        SelenideElement searchObject = $x("//div[@class='search-panel-content panel-content']//div[@class='modal-content']//form[@class='form-search']//div[@class='search-wrapper']//button[@class='search-submit']");
        searchObject.click();
        sleep(2000);

        return  this;

    }


    public SearchBuyProduct buyProduct(String nameProduct){
        SelenideElement product = $x("//div[@class='summary entry-summary']/h1[contains(text(),'$s')]".formatted(nameProduct));
        SelenideElement plus = $x("//div[@class='product-gallery-summary clearfix ']//form[@class='cart']/..//div[@class='quantity']//span[contains(@class,'button increase')]");
        plus.click();
        sleep(3000);
        SelenideElement addCart = $x("//div[@class='product-gallery-summary clearfix ']//form[@class='cart']/..//button[@name='add-to-cart']");
        addCart.click();

        return this;
    }

    public SearchBuyProduct transitionCart(){
        //SelenideElement buttonCart = $x("//div[contains(@class,'panel-content')]//div[@class='modal-content']/../../..//a[contains(@class,'razzi-button') and text()='Посмотреть корзину']");
        SelenideElement buttonCart = $x("//a[contains(@class,'razzi-button') and text()='Посмотреть корзину']");
        buttonCart.click();

        return this;
    }
}
