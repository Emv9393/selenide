package ru.smlab.pages;

import com.codeborne.selenide.ClickOptions;
import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.*;

public class ShoppingCart {

    public ShoppingCart decrease(String nameProduct){
        SelenideElement minus = $x("//td[descendant::a[contains(text(),'%s')]]//span[contains(@class,'decrease')]".formatted(nameProduct));
        minus.click(ClickOptions.usingJavaScript());
        sleep(2000);
        SelenideElement messageCart = $x("//div[@class='woocommerce-notices-wrapper']//div[@class='woocommerce-message' and contains(text(),'Корзина обновлена.')]");
        return this;
    }

    public ShoppingCart order(){
        SelenideElement order = $x("//div[@class='wc-proceed-to-checkout']/a[contains(@class,'checkout-button') and contains(text(),'Оформить заказ')]");
        order.click();

        return this;
    }
}
