package ru.smlab.pages;

import com.codeborne.selenide.SelenideElement;
import static com.codeborne.selenide.Selenide.*;

public class MakeAnOrder {

    public MakeAnOrder checkout(){
        SelenideElement title = $x("//div[@class='site']/div[@class='page-header ']/div[contains(@class,'page-header__content')]/h1[text()='Оформение заказа']");

        return this;
    }

    public MakeAnOrder detailsPayment(String nameClient , String familyNameClient){
        SelenideElement name = $x("//input[@name='billing_first_name']");
        name.setValue(nameClient);
        SelenideElement familyName = $x("//input[@name='billing_last_name']");
        familyName.setValue(familyNameClient);

        return this;
    }
}
